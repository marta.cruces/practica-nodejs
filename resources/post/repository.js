const Post = require('./model');

exports.getAllPosts = function () {
    return Post.find({}, 'author title text date').populate({ path: 'author', select: 'username -_id' }).exec();
}

exports.getPostById = function(postId) {
    return Post.findById(postId, 'author title text date comments').populate({ path: 'author', select: 'username _id' }).exec();
}

exports.createPost = function(body, userId) {
   return Post.create({ ...body, author: userId }).then((createdDocument) => {
    return createdDocument.populate({ path: 'author', select: 'username -_id' }).execPopulate();
   });
}

exports.deletePostById = function(postId) {
    return Post.findByIdAndDelete(postId).exec();
}

exports.modifyPost = function(postId, body) {
    return Post.findByIdAndUpdate(postId, body, { new: true });
}

exports.createComment = function(postId, body) {
    return Post.findByIdAndUpdate(postId, { $push: { comments: body } }, { new: true }).exec();
}

exports.modifyComment = function(postId, commentId, body) {
    return Post.findOneAndUpdate(
        { _id: postId, 'comments._id': commentId },
        { $set: {
            'comments.$.nickname': body.nickname || undefined,
            'comments.$.content': body.content
        } },
        { omitUndefined: true, new: true }
    ).exec();
}

exports.deleteComment = function(postId, commentId) {
    return Post.findByIdAndUpdate(postId, {$pull: { comments: { _id: commentId} } }, { new: true }).exec();
}